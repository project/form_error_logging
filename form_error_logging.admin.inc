<?php
/**
 * @file
 * Admin configuration implementation for "Form Error Logging" module.
 */

/**
 * Callback function for defining configuration form.
 */
function form_error_logging_config_form() {
  $form = array();
  $form['form_error_logging_forms'] = array(
    '#type' => 'textarea',
    '#title' => t('Whitelisted form IDs'),
    '#description' => t('Write form IDs that you want to track validation errors for. Each form in each line.'),
    '#default_value' => variable_get('form_error_logging_forms', ''),
  );
  return system_settings_form($form);
}
